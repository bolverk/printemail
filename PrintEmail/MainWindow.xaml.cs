﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MailKit.Net.Imap;
using System.Threading;
using System.IO;

namespace PrintEmail
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Configuration configuration = new Configuration();
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = configuration;
            this.password.Password = configuration.password; // Password box can't be bound

            System.Diagnostics.Debug.WriteLine("Creating Timer...");
            var aTimer = new IMAPTimer
            {
                Interval = 60 * 1000 * 1,
                Config = configuration,
                AutoReset = true,
                Enabled = true
            };
            aTimer.Elapsed += OnTimedEvent;
            aTimer.Start();
        }
        private static void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Timer run.");
            CheckIMAP(((IMAPTimer)sender).Config);
        }
        private static void CheckIMAP(Configuration configuration)
        {
            System.Diagnostics.Debug.WriteLine("In IMAP check routine.");
            if (configuration.valid == Configuration.TestStatus.Valid)
            {
                Task.Run( () =>
                {
                    System.Diagnostics.Debug.WriteLine("Fired IMAP check");
                    var client = new ImapClient();
                    client.Connect(configuration.serverName, configuration.portAsInt, configuration.sockOptions);

                    try
                    {
                        client.Authenticate(configuration.userName, configuration.password);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Authentication error!");
                    }

                    if (client.IsAuthenticated)
                    {
                        var inbox = client.Inbox;
                        inbox.Open(MailKit.FolderAccess.ReadWrite);

                        System.Diagnostics.Debug.WriteLine("Total messages: {0}", inbox.Count);
                        System.Diagnostics.Debug.WriteLine("Recent messages: {0}", inbox.Recent);

                        foreach (var uid in inbox.Search(MailKit.Search.SearchQuery.NotFlags(MailKit.MessageFlags.Seen)))
                        {
                            var message = inbox.GetMessage(uid);
                            System.Diagnostics.Debug.WriteLine("[match] {0}: {1}", uid, message.Subject);

                            EmailPrinter ep = new EmailPrinter(configuration.printerName);
                            MemoryStream ms = new MemoryStream();
                            message.Body.WriteTo(ms);
                            ms.Position = 0;
                            StreamReader sr = new StreamReader(ms);
                            ep.Body = sr.ReadToEnd();
                            ep.Subject = message.Subject;
                            ep.print();

                            inbox.AddFlags(uid, MailKit.MessageFlags.Seen, true);
                        };

                        client.Disconnect(true);
                    }
                });
            }
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void ShowWindowCommand(object sender, RoutedEventArgs e)
        {
            this.Show();
            if (this.WindowState == WindowState.Minimized)
                this.WindowState = WindowState.Normal;
        }
        private void Window_StateChanged(object sender, EventArgs e)
        {
            switch (this.WindowState)
            {
                case WindowState.Maximized:
                    break;
                case WindowState.Minimized:
                    this.Hide(); // Just the tray icon
                    break;
                case WindowState.Normal:
                    break;
            }
        }
        private void Printer_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog p = new PrintDialog();
            if (p.ShowDialog() == true)
            {
                configuration.printerName = p.PrintQueue.FullName;
            }
        }
        private void password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ((dynamic)this.DataContext).password = ((PasswordBox)sender).Password;
            }
        }

        public bool imap_test()
        {
            var client = new ImapClient();

            try
            {
                client.Connect(configuration.serverName, configuration.portAsInt, configuration.sockOptions);
                client.Authenticate(configuration.userName, configuration.password);
            }
            catch (Exception e)
            {
                configuration.valid = Configuration.TestStatus.Failed;
                return false;
            }
            if (client.IsAuthenticated)
            {
                configuration.valid = Configuration.TestStatus.Valid;
                return true;
            }
            else
            {
                configuration.valid = Configuration.TestStatus.Failed;
                return false;
            }
        }
        private void Test_Click(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
           {
               imap_test();
           });
        }
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            textBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }
    }
}
