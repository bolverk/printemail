﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Security;
using Microsoft.Win32;

namespace PrintEmail
{
    class Configuration : INotifyPropertyChanged
    {
        const String RegSubKey = @"LoyaltyForge\PrintEmail";

        public enum TestStatus
        {
            Untested = 0,
            Valid = 1,
            Failed = -1
        }

        private string serverNameValue;
        private int portValue;
        private string userNameValue;
        private string passwordValue;
        private string encMethodValue;
        private string authMethodValue;
        private string printerNameValue;
        private TestStatus validValue;
        private RegistryKey regKey;

        public Configuration()
        {
            regKey = Registry.CurrentUser.CreateSubKey(RegSubKey);
            if (regKey is null)
            {
                // TODO: Raise an exception
            }

            serverNameValue = regKey.GetValue("ServerName", "server.example.com")?.ToString();
            portValue = 143;
            int.TryParse(regKey.GetValue("Port", "143")?.ToString(), out portValue);
            userNameValue = regKey.GetValue("UserName", "")?.ToString();
            passwordValue = DecodeBase64(System.Text.Encoding.UTF8, regKey.GetValue("Password", null)?.ToString());
            encMethodValue = regKey.GetValue("EncMethod", "STARTTLS")?.ToString();
            authMethodValue = regKey.GetValue("AuthMethod", "Normal")?.ToString();
            printerNameValue = regKey.GetValue("PrinterName", null)?.ToString();
            validValue = (TestStatus)Enum.Parse(typeof(TestStatus), regKey.GetValue("Tested", 0).ToString());
        }
        public TestStatus valid
        {
            get
            { return validValue; }
            set
            {
                validValue = value;
                regKey.SetValue("Tested", (int)value);
                NotifyPropertyChanged("valid");
                NotifyPropertyChanged("validString");
            }
        }
        public string validString
        {
            get
            {
                switch (validValue)
                {
                    case TestStatus.Untested:
                        return "Not tested.";
                    case TestStatus.Valid:
                        return "Valid.";
                    case TestStatus.Failed:
                        return "Failed.";
                    default:
                        return "Untested";
                }
            }
        }
        public string serverName
        {
            get
            { return serverNameValue; }
            set
            {
                if (serverNameValue != value)
                {
                    this.valid = TestStatus.Untested;
                    serverNameValue = value;
                    regKey.SetValue("ServerName", value);
                    NotifyPropertyChanged("serverName");
                }
            }
        }
        public string port
        {
            get { return portValue.ToString(); }
            set
            {
                int r = portValue;
                if (int.TryParse(value, out r))
                    if (portValue != r)
                    {
                        this.valid = TestStatus.Untested;
                        portValue = r;
                        regKey.SetValue("Port", value);
                        NotifyPropertyChanged("port");
                        NotifyPropertyChanged("portAsInt");
                    }
            }
        }
        public int portAsInt
        {
            get { return portValue; }
        }
        public string userName
        {
            get { return userNameValue; }
            set
            {
                if (userNameValue != value)
                {
                    this.valid = TestStatus.Untested;
                    regKey.SetValue("UserName", value);
                    userNameValue = value;
                }
            }
        }
        public string password
        {
            get { return passwordValue; }
            set
            {
                if (passwordValue != value)
                {
                    this.valid = TestStatus.Untested;
                    passwordValue = value;
                    regKey.SetValue("Password", EncodeBase64(System.Text.Encoding.UTF8, value));
                    NotifyPropertyChanged("password");
                }
            }
        }
        public string encMethod
        {
            get { return encMethodValue; }
            set {
                if (encMethodValue != value)
                {
                    this.valid = TestStatus.Untested;
                    encMethodValue = value;
                    regKey.SetValue("EncMethod", value);
                    NotifyPropertyChanged("encMethod");
                }
            }
        }
        public string authMethod
        {
            get { return authMethodValue; }
            set
            {
                if (authMethodValue != value)
                {
                    this.valid = TestStatus.Untested;
                    authMethodValue = value;
                    regKey.SetValue("AuthMethod", value);
                    NotifyPropertyChanged("authMethod");
                }
            }
        }
        public string printerName
        {
            get { 
                if (printerNameValue == null)
                    return "Unconfigured";
                return printerNameValue;
            }
            set
            {
                if (printerNameValue != value)
                {
                    printerNameValue = value;
                    regKey.SetValue("PrinterName", value);
                    NotifyPropertyChanged("printerName");
                }
            }
        }
        public MailKit.Security.SecureSocketOptions sockOptions
        {
            get
            {
                switch (encMethodValue)
                {
                    case "SSL":
                        return MailKit.Security.SecureSocketOptions.SslOnConnect;
                    case "STARTTLS":
                        return MailKit.Security.SecureSocketOptions.StartTls;
                    default:
                        return MailKit.Security.SecureSocketOptions.Auto;
                }
            }
        }
        // Encoding helpers
        public static string EncodeBase64(System.Text.Encoding encoding, string text)
        {
            if (text == null)
            {
                return null;
            }

            byte[] textAsBytes = encoding.GetBytes(text);
            return System.Convert.ToBase64String(textAsBytes);
        }
        public static string DecodeBase64(System.Text.Encoding encoding, string encodedText)
        {
            if (encodedText == null)
            {
                return null;
            }

            byte[] textAsBytes = System.Convert.FromBase64String(encodedText);
            return encoding.GetString(textAsBytes);
        }

        // Need to implement this interface in order to get data binding to work properly.
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
