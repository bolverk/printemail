﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Printing;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Text.RegularExpressions;
using HTMLConverter;
using System.IO;

namespace PrintEmail
{
    class EmailPrinter
    {
        private FlowDocument doc;
        private PrintDialog pd;

        public string Body;
        public string Subject;

        public EmailPrinter(string printerPath)
        {
            PrintQueue pq = null;
            foreach(string printerName in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                System.Diagnostics.Debug.WriteLine("Found printer " + printerName);
                if (printerName == printerPath)
                {
                    System.Diagnostics.Debug.WriteLine("Matched printer " + printerName);
                    var match = Regex.Match(printerName, @"(?<machine>\\\\.*?)\\(?<queue>.*)");
                    if (match.Success)
                    {
                        pq = new PrintServer(match.Groups["machine"].Value).GetPrintQueue(match.Groups["queue"].Value);
                    }
                    else
                    {
                        pq = new LocalPrintServer().GetPrintQueue(printerName);
                    }
                }
            }
            if (pq != null)
            {
                pd = new PrintDialog();
                pd.PrintQueue = pq;
                doc = new FlowDocument();
            }
            else
            {
                throw new Exception();
            }
        }

        public void print()
        {
            if (doc == null || pd == null)
                return; // Consider throwing an exception

            doc.Name = "EmailPrinter";

            Section header = new Section();
            Paragraph p1 = new Paragraph();
            Bold bld = new Bold();
            bld.Inlines.Add(new Run("Subject: "));
            p1.Inlines.Add(bld);
            p1.Inlines.Add(new Run(Subject));

            header.Blocks.Add(p1);
            doc.Blocks.Add(header);

            Section body = new Section();
            string xaml = HtmlToXamlConverter.ConvertHtmlToXaml(Body, true);
            // Strip tags with no body
            xaml = Regex.Replace(xaml, @"<[^>/][^>]*/ *>", string.Empty);

            FlowDocument fd2 = XamlReader.Parse(xaml) as FlowDocument;
            while (fd2.Blocks.Count > 0)
            {
                var block = fd2.Blocks.FirstBlock;
                fd2.Blocks.Remove(block);
                body.Blocks.Add(block);
            }

            doc.Blocks.Add(body);

            DocumentPaginator dp = ((IDocumentPaginatorSource)doc).DocumentPaginator;
            FittedDocumentPaginator fdp = new FittedDocumentPaginator(dp, .725);

            pd.PrintDocument(fdp, "Email Printing");
        }
    }
}
